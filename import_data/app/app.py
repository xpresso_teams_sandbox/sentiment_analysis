"""
This will be used to import datasets
Used for Data: Movie Reviews
"""
__author__ = "Aditya Aggarwal"

import json
import pandas as pd

from import_data.app.helperfile import data_read

if __name__ == '__main__':
  # reading config file
  try:
    with open('../config/dev.json') as config_file:
      data = json.load(config_file)
    TRAIN_PATH = data["TRAIN_PATH"]

    # reading training data
    pos_results = data_read(TRAIN_PATH, 'train', 'pos')
    reviews = pos_results[0]
    labels = pos_results[1]
    neg_results = data_read(TRAIN_PATH, 'train', 'neg')
    reviews += neg_results[0]
    labels += neg_results[1]
    train_df = pd.DataFrame({'reviews': reviews, 'y': labels})

    # saving training data for preprocessing
    train_df.to_pickle('../../preprocess_data/data/train_df')
    print("training file saved in preprocess_data/data folder")
  except:
    print("Error while import ... ")