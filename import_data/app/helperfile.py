import os


def data_read(path, train_or_test, pos_or_neg):
    '''
    Currently only for binary classifier where positive data is kept in one folder and negative data in another
    path: path to train or test data
    train_or_test: specify if it is test dataset or train dataset
    pos_or_neg: target value is positive or negative
    '''
    reviews, labels = [], []

    for filename in os.listdir(path + '/' + pos_or_neg):
        with open(path + '/' + pos_or_neg + '/' + filename, encoding="utf8") as f:
            reviews.append(f.read())
        if pos_or_neg == 'pos':
            label = 1
        else:
            label = 0
        labels.append(label)

    return reviews, labels
