"""
It will take data prepared in import step from data folder
"""
__author__ = "Aditya Aggarwal"

import pandas as pd
import pickle

from preprocess_data.app.helperfile import Voc, ReviewDataset

if __name__ == '__main__':
  try:
    #read config file

    # reading file
    train_df = pd.read_pickle("../data/train_df", compression=None)

    reviews = Voc('reviews')
    for review in train_df['reviews']:
      reviews.add_sentence(review)

    train_ds = ReviewDataset(train_df)
    test_ds = ReviewDataset(train_df)
    pickle.dump(reviews, open('../../train/data/reviews.pkl', 'wb'))
    print("reviews as a pickle saved into train/data...")
  except:
    print("error in preprocessing ... .. ")