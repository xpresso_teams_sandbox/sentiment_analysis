
import json
from torch.utils.data import Dataset, DataLoader

with open('../config/dev.json') as config_file:
    data = json.load(config_file)
UNK_token = data["UNK_token"]
PAD_token = data["PAD_token"]
SOS_token = data["SOS_token"]
EOS_token = data["EOS_token"]

class Voc(Dataset):
    def __init__(self, name):
        self.name = name
        self.trimmed = False
        self.word2index = {}
        self.word2count = {}
        self.index2word = {PAD_token: 'PAD', SOS_token: 'SOS',
                           EOS_token: 'EOS', UNK_token: 'UNK'}
        self.num_words = 4 # include the ones above

    def add_sentence(self, sentence):
        for word in sentence.split():
            self.add_word(word.lower())

    def add_word(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.num_words
            self.word2count[word] = 1
            self.index2word[self.num_words] = word
            self.num_words += 1
        else:
            self.word2count[word] += 1

    # remove words that appear less frequently
    def trim(self, min_count):
        if self.trimmed:
            return
        self.trimmed = True
        keep_words = []
        for k, v in self.word2count.items():
            if v >= min_count:
                keep_words.append(k)

        self.word2index = {}
        self.word2count = {}
        self.index2word = {PAD_token: 'PAD', SOS_token: 'SOS',
                           EOS_token: 'EOS', UNK_token: 'UNK'}
        self.num_words = 4
        for word in keep_words:
            self.add_word(word)

# takes string sentence and returns sentence of word indices
def indexesFromSentence(voc, sentence):
    return [voc.word2index[word] for word in sentence.split()] + [EOS_token]


class ReviewDataset():
    def __init__(self, df):
        """Bad coding practice to use the globals but it will work for now"""
        self.reviews = df
        self.text = df['reviews']
        self.rating = df['y']

    def __len__(self):
        return len(self.reviews)

    def __getitem__(self, idx):
        return (self.text[idx], self.rating[idx])

